var _ = require('lodash');
var InvalidArgumentError = require('oauth2-server/lib/errors/invalid-argument-error');
var InvalidClientError = require('oauth2-server/lib/errors/invalid-client-error');
var InvalidRequestError = require('oauth2-server/lib/errors/invalid-request-error');
var OAuthError = require('oauth2-server/lib/errors/oauth-error');
var ServerError = require('oauth2-server/lib/errors/server-error');
var UnauthorizedRequestError = require('oauth2-server/lib/errors/unauthorized-request-error');
var Promise = require('bluebird');
var promisify = require('promisify-any').use(Promise);
var auth = require('basic-auth');
var crypto = require('crypto');
var randomBytes = require('bluebird').promisify(crypto.randomBytes);
var model = require("./model")

var Request = require('oauth2-server').Request;
var Response = require('oauth2-server').Response;
var is = require('oauth2-server/lib/validator/is');

MyAuth.prototype.token = function (request, response, options, callback) {
    return handle(request, response, options)
        .nodeify(callback);
}

MyAuth.prototype.thirdPartyToken = function (options) {
    var that = this;

    return function (req, res, next) {
        var request = new Request(req);
        var response = new Response(res);

        return Promise.bind(that)
            .then(function () {
                return this.token(request, response, options);
            })
            .tap(function (token) {
                res.locals.oauth = {
                    token: token
                };
            })
            .then(function () {
                return handleResponse.call(this, req, res, response);
            })
            .catch(function (e) {
                return handleError.call(this, e, req, res, response, next);
            });
    };
};



var handle = function (request, response, options) {

    handleCheck(request, response, options);

    return Promise.bind({})
        .then(function () {
            return getClient(request, response, options);
        })
        .then(function (client) {
            return handleGrantType(request, client, options);
        })
        .tap(function (data) {
            var tokenType = {
                access_token: data.accessToken,
                token_type: 'Bearer'
            };
            if (options.accessTokenLifetime) {
                tokenType.expires_in = options.accessTokenLifetime;
            }
            if (data.refreshToken) {
                tokenType.refresh_token = data.refreshToken;
            }

            updateSuccessResponse(response, tokenType);
        }).catch(function (e) {
            if (!(e instanceof OAuthError)) {
                e = new ServerError(e);
            }
            updateErrorResponse(response, e);
            throw e;
        });
}


var getClientCredentials = function (request) {
    var credentials = auth(request);
    if (credentials) {
        return {
            clientId: credentials.name,
            clientSecret: credentials.pass
        };
    }

    if (request.body.client_id && request.body.client_secret) {
        return {
            clientId: request.body.client_id,
            clientSecret: request.body.client_secret
        };
    }

    throw new InvalidClientError('Invalid client: cannot retrieve client credentials');
};

var getClient = function (request, response, options) {
    var credentials = getClientCredentials(request);


    if (!credentials.clientId) {
        throw new InvalidRequestError('Missing parameter: `client_id`');
    }

    if (!credentials.clientSecret) {
        throw new InvalidRequestError('Missing parameter: `client_secret`');
    }

    if (!is.vschar(credentials.clientId)) {
        throw new InvalidRequestError('Invalid parameter: `client_id`');
    }

    if (credentials.clientSecret && !is.vschar(credentials.clientSecret)) {
        throw new InvalidRequestError('Invalid parameter: `client_secret`');
    }

    return promisify(options.model.getClient, 2).call(options.model, credentials.clientId, credentials.clientSecret)
        .then(function (client) {
            if (!client) {
                throw new InvalidClientError('Invalid client: client is invalid');
            }

            if (!client.grants) {
                throw new ServerError('Server error: missing client `grants`');
            }

            if (!(client.grants instanceof Array)) {
                throw new ServerError('Server error: `grants` must be an array');
            }

            return client;
        })
        .catch(function (e) {
            // Include the "WWW-Authenticate" response header field if the client
            // attempted to authenticate via the "Authorization" request header.
            //
            // @see https://tools.ietf.org/html/rfc6749#section-5.2.
            if ((e instanceof InvalidClientError) && request.get('authorization')) {
                response.set('WWW-Authenticate', 'Basic realm="Service"');

                throw new InvalidClientError(e, {
                    code: 401
                });
            }

            throw e;
        });
};

var handleGrantType = function (request, client, options) {

    var accessTokenLifetime = client.accessTokenLifetime || __accessTokenLifetime;
    var refreshTokenLifetime = client.refreshTokenLifetime || __refreshTokenLifetime;;
    options.accessTokenLifetime = accessTokenLifetime;
    options.refreshTokenLifetime = refreshTokenLifetime;

    return typeHandle(options, request, client);
};

var typeHandle = function (options, request, client) {
    if (!request) {
        throw new InvalidArgumentError('Missing parameter: `request`');
    }

    if (!client) {
        throw new InvalidArgumentError('Missing parameter: `client`');
    }


    return Promise.bind(this)
        .then(function () {
            return getThirdPartyUser(options, request);
        })
        .then(function (user) {
            if (!user) {
                return createThirdPartyUser(options, request);
            }
            return user;
        })
        .then(function (user) {
            return saveToken(user, client, options);
        });
};

var getThirdPartyUser = function (options, request) {
    if (!request.body.userId) {
        throw new InvalidRequestError('Missing parameter: `user`');
    }
    if (!is.uchar(request.body.userId)) {
        throw new InvalidRequestError('Invalid parameter: `user`');
    }
    if (!request.body.username) {
        throw new InvalidRequestError('Missing parameter: `username`');
    }
    if (!is.uchar(request.body.username)) {
        throw new InvalidRequestError('Invalid parameter: `username`');
    }
    return promisify(options.model.getThirdPartyUser, 2).call(options.model, request.body.userId, request.body.username)
        .then(function (user) {
            return user;
        });
};


var createThirdPartyUser = function (options, request) {
    if (!request.body.userId) {
        throw new InvalidRequestError('Missing parameter: `userId`');
    }

    if (!is.uchar(request.body.userId)) {
        throw new InvalidRequestError('Invalid parameter: `userId`');
    }
    if (!request.body.username) {
        throw new InvalidRequestError('Missing parameter: `username`');
    }

    if (!is.uchar(request.body.username)) {
        throw new InvalidRequestError('Invalid parameter: `username`');
    }

    if (!request.body.type) {
        throw new InvalidRequestError('Missing parameter: `username`');
    }

    if (!is.uchar(request.body.type) && type != 'google' && type != 'facebook') {
        throw new InvalidRequestError('Invalid parameter: `username`');
    }

    return promisify(options.model.createThirdPartyUser, 7).call(options.model,
            request.body.userId,
            request.body.firstname,
            request.body.lastname,
            request.body.username,
            request.body.email,
            request.body.photoUrl,
            request.body.type
        )
        .then(function (user) {
            if (!user) {
                throw new InvalidGrantError('Invalid grant: user credentials are invalid');
            }

            return user;
        });
};

var saveToken = function (user, client, options) {

    var accessTokenLifetime = options.accessTokenLifetime;
    var refreshTokenLifetime = options.refreshTokenLifetime;


    var fns = [
        generateAccessToken(),
        generateRefreshToken(),
        getAccessTokenExpiresAt(accessTokenLifetime),
        getRefreshTokenExpiresAt(refreshTokenLifetime)
    ];

    return Promise.all(fns)
        .bind()
        .spread(function (accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) {
            var token = {
                accessToken: accessToken,
                accessTokenExpiresAt: accessTokenExpiresAt,
                refreshToken: refreshToken,
                refreshTokenExpiresAt: refreshTokenExpiresAt
            };
            return promisify(model.saveToken, 3).call(model, token, client, user);
        });

};

var handleCheck = function (request, response, options) {
    // if (!options.accessTokenLifetime) {
    //     throw new InvalidArgumentError('Missing parameter: `accessTokenLifetime`');
    // }

    if (!options.model) {
        throw new InvalidArgumentError('Missing parameter: `model`');
    }

    

    if (!options.model.getClient) {
        throw new InvalidArgumentError('Invalid argument: model does not implement `getClient()`');
    }


    if (!options.model.getUser) {
        throw new InvalidArgumentError('Invalid argument: model does not implement `getUser()`');
    }

    if (!options.model.saveToken) {
        throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
    }

    if (!(request instanceof Request)) {
        throw new InvalidArgumentError('Invalid argument: `request` must be an instance of Request');
    }

    if (!(response instanceof Response)) {
        throw new InvalidArgumentError('Invalid argument: `response` must be an instance of Response');
    }

    if (request.method !== 'POST') {
        throw new InvalidRequestError('Invalid request: method must be POST');

    }

    if (!request.is('application/x-www-form-urlencoded')) {
        throw new InvalidRequestError('Invalid request: content must be application/x-www-form-urlencoded');
    }
}



var updateSuccessResponse = function (response, tokenType) {
    response.body = tokenType.valueOf();

    response.set('Cache-Control', 'no-store');
    response.set('Pragma', 'no-cache');
};


var updateErrorResponse = function (response, error) {
    response.body = {
        error: error.name,
        error_description: error.message
    };
    response.status = error.code;
};




var getRefreshTokenExpiresAt = function (refreshTokenLifetime) {
    var expires = new Date();
    expires.setSeconds(expires.getSeconds() + refreshTokenLifetime);
    return expires;
};

var getAccessTokenExpiresAt = function (accessTokenLifetime) {
    var expires = new Date();
    expires.setSeconds(expires.getSeconds() + accessTokenLifetime);
    return expires;
};

var generateAccessToken = function () {
    return randomBytes(256).then(function (buffer) {
        return crypto
            .createHash('sha1')
            .update(buffer)
            .digest('hex');
    });
};

var generateRefreshToken = function () {
    return randomBytes(256).then(function (buffer) {
        return crypto
            .createHash('sha1')
            .update(buffer)
            .digest('hex');
    });
};




/**
 * Handle response.
 */
var handleResponse = function (req, res, response) {

    if (response.status === 302) {
        var location = response.headers.location;
        delete response.headers.location;
        res.set(response.headers);
        res.redirect(location);
    } else {
        res.set(response.headers);
        res.status(response.status).send(response.body);
    }
};

/**
 * Handle error.
 */

var handleError = function (e, req, res, response, next) {

    if (this.useErrorHandler === true) {
        next(e);
    } else {
        if (response) {
            res.set(response.headers);
        }

        res.status(e.code);

        if (e instanceof UnauthorizedRequestError) {
            return res.send();
        }

        res.send({
            error: e.name,
            error_description: e.message
        });
    }
};

/**
 * Constructor.
 */

function MyAuth() {}

module.exports = MyAuth;