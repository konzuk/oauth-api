
/* ------------------- start socket -------------*/
var io = require('socket.io')(server);

io.use(function(socket, next){	
	if (socket.handshake.query && socket.handshake.query.auth){
		jwt.verify(socket.handshake.query.auth, 'kh$ty1e', function(err, decoded) { 
			if(err) return next(new Error('Authentication error'));
			db.connect(function(cnn){
				var sql = 'SELECT clID FROM app_client_register WHERE clID = ? LIMIT 1';
				cnn.query(sql,[decoded.clID], function(err, rows, fields){
					if(err) throw err;
					if(rows.length == 1){
						socket.clID = decoded.clID;
						next();

						var dt = datetime.create();
						var dNow = dt.format('Y-m-d H:M:S');

						var update = "UPDATE app_client_register SET clLastLoginDate = ?,clActive = 1 WHERE clID = ? LIMIT 1";
						cnn.query(update,[dNow,socket.clID], function(err, rows, fields){
							if(err) throw err;
							console.log('user <'+socket.clID+'> Online');
						});
						return;
					}else{
						resp.error(req,res,401,"INVALID_TOKEN");
					}
				});
			});
			//console.log(os.hostname());
			//next(); return;
		});
	}
})
.on('connection', function(socket) {
	var clients = io.sockets.connected;
    console.log("connected : Total " + Object.keys(clients).length);
	socket.broadcast.emit('online_offline',{"active":1,"clID":socket.clID});
	socket.on('seen', function(data) {
		socket.broadcast.emit('seen_'+data.receiver.clID, data);
	});
	socket.on('typing', function(data) {
		socket.broadcast.emit('typing_'+data.receiver.clID, data);
	});
	socket.on('stoptyping', function(data) {
		socket.broadcast.emit('stoptyping_'+data.receiver.clID, data);
	});
    socket.on('sent', function(data) {

		var dt = datetime.create();
		var dNow = dt.format('Y-m-d H:M:S');				
		db.connect(function(cnn){
			var inser_chat = function(chat_room){
				data.message = data.message.replace(/</g, "&lt;").replace(/>/g,"&gt;");  

				var dInsert = {
					chat_room_id : chat_room,
					cht_receiver_id : data.receiver.clID,
					cht_sender_id : data.sender.clID,
					cht_sent_date : dNow,
					cht_message : data.message,
					cht_status : 1,
					cht_sender_hide : 0,
					cht_receiver_hide : 0,
					cht_seen : 0
				};
				cnn.query("INSERT INTO chat_message SET ?",dInsert, function(err, rows, fields){
					if(err) throw err;
					data.chat_id = rows.insertId;
					data.chat_date = dNow;
					console.log('receive_'+data.receiver.clID, data);
					socket.broadcast.emit('receive_'+data.receiver.clID, data);
				});
			}
			var insert_member = function(member){
				cnn.query("INSERT INTO chat_member SET ?",member, function(err2, rows2, fields){
					if(err2) throw err2;
				});
			};
			var sqlCheck = 'SELECT chat_room_id FROM chat_message WHERE (cht_sender_id = ? AND cht_receiver_id = ?) OR (cht_sender_id = ? AND cht_receiver_id = ?) LIMIT 1';
			cnn.query(sqlCheck,[data.sender.clID,data.receiver.clID,data.receiver.clID,data.sender.clID], function(err1, rows1, fields){
				if(rows1.length == 1){
					inser_chat( rows1[0].chat_room_id );
					return;
				}else{
					cnn.query("INSERT INTO chat_room SET ?",{chr_created_at:dNow,chr_updated_at:dNow}, function(err2, rows2, fields){
						if(err2) throw err2;
						inser_chat( rows2.insertId );
						insert_member({chat_room_id:rows2.insertId,chm_member_id:data.sender.clID});
						insert_member({chat_room_id:rows2.insertId,chm_member_id:data.receiver.clID});
					});
				}
			});			

		});
    });
	socket.on('disconnect', function(err) {
		db.connect(function(cnn){
			var dt = datetime.create();
			var dNow = dt.format('Y-m-d H:M:S');
			var update = 'UPDATE app_client_register SET clLastLoginDate = ?,clActive = 0 WHERE clID = ? LIMIT 1';
			cnn.query(update,[dNow,socket.clID], function(err, rows, fields){
				console.log('user '+socket.clID+ " Offline");
				socket.broadcast.emit('online_offline',{"active":0,"clID":socket.clID,"date":dNow});
				if(err) throw err;
			});
		});
    });
});