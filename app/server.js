'use strict';

// Third Party
var express = require('express');
var bodyParser = require('body-parser');
var oauthserver = require('express-oauth-server');
var mongoose = require('mongoose');
var datetime = require('node-datetime');
var os = require('os');
var fs = require('fs');
var cors = require('cors');
var http = require("http");

var Promise = require('bluebird');
var promisify = require('promisify-any').use(Promise);


// Class
var msg = require(__base + 'app/config/response');
var model = require(__base + 'app/model');
var myAuth = require('./myAuth');


var app = express();
var server = http.createServer(app);

app.use(cors());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
server.listen(__server_port, __server, function () {
	console.log("Server listening at port %d", __server_port);
});

mongoose.connect(__mongo_uri, {
	useNewUrlParser: true
}, function (err, res) {
	if (err) {
		return console.error('Error connecting to "%s":', __mongo_uri, err);
	}
	console.log('Connected successfully to "%s"', __mongo_uri);
});

app.oauth = new oauthserver({
	model: model,
	grants: ['password', 'refresh_token'],
	debug: true
});

var auth = new myAuth();

var opt = {
	model: model
};

app.all('/oauth/token', app.oauth.token());
app.all('/oauth/third-party', auth.thirdPartyToken(opt));

app.post('/oauth/logout', function (req, res) {
	if (req.body.token) {
		model.revokeToken({
			refreshToken: req.body.token
		});
	}
	msg.json(req, res, 'Logout !!!')
});

app.get('/starter', app.oauth.authenticate(), function (req, res) {
	var data = {
		message: 'Congratulations, you are in a secret area!'
	};
	msg.json(req, res, data);
});




// app.route('/v1/api/:models/:method')
// 	.get(auth.secureHead, function (req, res) {
// 		api_access(req, res, 'simple');
// 	})
// 	.post(auth.secureHead, function (req, res) {
// 		api_access(req, res, 'simple');
// 	});
// app.route('/v1/api/:models/:method/:opt')
// 	.get(auth.secureHead, function (req, res) {
// 		api_access(req, res, 'simple');
// 	})
// 	.post(auth.secureHead, function (req, res) {
// 		api_access(req, res, 'simple');
// 	});

// app.route('/v1/auth/:models/:method')
// 	.get(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	})
// 	.post(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	});

// 	app.route('/v1/auth/:models/:method')
// 	.get(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	})
// 	.post(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	});
// app.route('/v1/auth/:models/:method/:opt')
// 	.get(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	})
// 	.post(auth.checkToken, function (req, res) {
// 		api_access(req, res, 'admin');
// 	});

// function api_access(req, res, folder) {
// 	res.setHeader('Access-Control-Allow-Origin', '*');
// 	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, CONNECT');
// 	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
// 	res.setHeader('Access-Control-Allow-Credentials', true);
// 	res.setHeader('Access-Control-Max-Age', '86400'); // 24 hours
// 	res.setHeader('X-Powered-By', 'AZE Tech');

// 	var models = req.params.models;
// 	var UrlMethod = req.params.method;
// 	var path = __base + "app/cdr/" + folder + "/";

// 	fs.exists(path + models + ".js", function (exists) {
// 		if (exists) {
// 			var appMDL = require(path + models);
// 			if (!appMDL.hasOwnProperty(UrlMethod)) {
// 				console.log(path);
// 				msg.error(req, res, 405, "Method not allow allow");
// 				return;
// 			} else {
// 				var UrlOpt = req.params.opt || "";
// 				appMDL[UrlMethod](req, res, UrlOpt);
// 			}
// 			console.log(UrlMethod);
// 		} 
// 		else {
// 			msg.error(req, res, 404, "Endpoint not exit");
// 			console.error.bind(console);
// 		}
// 	});
// }

app.use(function (req, res, next) {
	msg.error(req, res, 404, "Wrong End Point");
});