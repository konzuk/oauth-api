
exports.error = function (req, res, number, err, data) {
	var err = err || "Error";
	console.log(data);

	if (data) {
		data = [];
	} else {
		data = {}
	}

	var msg = {
		'status': false,
		'statusCode': number,
		'message': err,
		'data': data,
		'type': 'application/json',
		'body': err
	};
	msg.body = JSON.stringify(msg);

	res.setHeader('x-message', err);
	res.writeHead(msg.statusCode, msg.message, {
		'Content-Type': msg.type
	});
	res.write(msg.body);
	res.end();
};
exports.msg = function (req, res, msg) {
	res.writeHead(500, "Warming", {
		'Content-Type': "application/json"
	});
	if (msg) {
		var returndata = {
			'status': false,
			'error': msg
		};
		res.write(JSON.stringify(returndata));
	}
	res.end();
};

exports.json = function (req, res, data) {
	res.writeHead(200, {
		'Content-Type': "application/json"
	});
	if (data) {
		var returndata = {
			'status': true,
			'statusCode': 200,
			'data': data
		};
		res.write(JSON.stringify(returndata));
	}
	res.end();
};
