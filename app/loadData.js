
var loadExampleData = function () {
	var OAuthClientsModel = mongoose.model('OAuthClients');
	var OAuthUsersModel = mongoose.model('OAuthUsers');

	var client = new OAuthClientsModel({
		clientId: __client_id,
		clientSecret: __client_secret,
		grants: ['password', 'refresh_token']
    });
    
  

	var user = new OAuthUsersModel({
		id: '123',
		username: 'pedroetb',
		password: 'password'
    });
    
    client.save(function (err, client) {

		if (err) {
			return console.error(err);
		}
		console.log('Created client', client);
	});


	user.save(function (err, user) {

		if (err) {
			return console.error(err);
		}
		console.log('Created user', user);
	});
};

// loadExampleData();