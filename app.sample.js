global.__base = __dirname + '/';
global.__server = 'localhost';
global.__server_port = '3000';
global.__server_base = __server + ":" + __server_port;
global.__mongo_uri = 'mongodb://localhost:27017/oauth';

global.__accessTokenLifetime = 60 * 60;    // 1hour
global.__refreshTokenLifetime = 60 * 60 * 24 * 14;  // 2 weeks.

global.__client_id = 'application';
global.__client_secret = 'secret';
// //var mongoUri = 'mongodb://user:password@178.128.210.188:27017/oauth?authSource=admin';
require(__base + 'app/server');
